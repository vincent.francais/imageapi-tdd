[![pipeline status](https://gitlab.com/vincent.francais/imageapi-tdd/badges/master/pipeline.svg)](https://gitlab.com/vincent.francais/imageapi-tdd/commits/master)

# ImageAPI-TDD
This is a small python3 REST API built using flask. It is part of a recruitment process.
The API allows to post and get an image, get the EXIF metadata of an image and get the most similar image of a given one. The images are stored on the filesystem.

#### Usage

The API is built in a Docker container. 
The routes available are:

| Endpoint | HTTP | CRUD | Function |
|-----|-----|-----|----------|
| /images/:id | GET | READ | Get a single image|
|/images|GET|READ|Get a list of all images in db|
|/images|POST|CREATE|Add an image|
|/images/:id/similar| GET | READ | Get the most similar image|
|/images/:id/metadata| GET | READ | Get the image's metadata |

To POST an image, its data must be stored in the 'file' field of a multipart/form-data.
Exemple:
```python
import request
image_file = {'file': open(img_path, 'rb')}
requests.post('url/images', files=image_file)
```

#### Similarity computation
To find the most similar image we first compute the features vector using an image classifier convolutional neural network. Currently we use Keras/TF and the MobileNetv2 CNN pretrained on the ImageNet dataset.
Then the image whose vector minimizes the L2 distance is the most similar

#### Test instance
A running instance of the API is available on heroku: https://enigmatic-journey-81752.herokuapp.com/images
As Heroku's file system is ephemeral the image stored in the database may actualy not be stored in the FS. To drop and recreate the DB tables use
```python
python manage.py recreate_db
```