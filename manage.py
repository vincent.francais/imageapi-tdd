'''
File: /home/vincent/Documents/Projets/imageAPI-tdd/manage.py
Created Date: Tuesday December 10th 2019
Author: Vincent Français
-----
Last Modified:
Modified By:
-----
'''

import sys
import os
from shutil import copy2
from imghdr import what

from flask.cli import FlaskGroup
from project import create_app, db
from project.api.images.models import Image
from project.api.features_extraction import MobileNet

app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command('recreate_db')
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command('seed_db')
def seed_db():
    """
    Seed the database with some images.
    """
    if not os.path.exists(app.config['SAVE_PATH']):
        os.makedirs(app.config['SAVE_PATH'])

    model = MobileNet()

    test_image_dir = './project/tests/test-images/'
    images_list = os.listdir(test_image_dir)

    for filename in images_list:
        if what(os.path.join(test_image_dir, filename)) is None:
            continue
        app.logger.debug(f'seeding image {filename} to database')
        vec = model.get_features_vector(os.path.join(test_image_dir, filename))
        db.session.add(Image(filename=filename, uri=filename, vec=vec))

        copy2(os.path.join(test_image_dir, filename),
              os.path.join(app.config['SAVE_PATH'], filename))

    db.session.commit()


if __name__ == '__main__':
    cli()
