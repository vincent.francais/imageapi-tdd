"""
File: /home/vincent/Documents/Projets/imageAPI-tdd/project/__init__.py
Created Date: Tuesday December 10th 2019
Author: Vincent Français
-----
Last Modified:
Modified By:
-----
"""
import os
from flask import Flask
from flask_admin import Admin
from flask_sqlalchemy import SQLAlchemy

# instanciate the db
db = SQLAlchemy()

admin = Admin(template_mode="bootstrap3")


def create_app(script_info=None):
    # instanciate the app
    app = Flask(__name__)

    # set config
    app_settings = os.getenv("APP_SETTINGS")
    app.config.from_object(app_settings)

    # set up extensions
    db.init_app(app)

    if os.getenv("FLASK_ENV") == "development":
        admin.init_app(app)

    # register blueprints
    from project.api.ping import ping_bp

    app.register_blueprint(ping_bp)
    from project.api.images.views import images_bp

    app.register_blueprint(images_bp)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    return app
