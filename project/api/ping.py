"""
File: /home/vincent/Documents/Projets/imageAPI-tdd/project/api/ping.py
Created Date: Tuesday December 10th 2019
Author: Vincent Français
-----
Last Modified:
Modified By:
-----
"""


from flask import Blueprint
from flask_restful import Api, Resource

ping_bp = Blueprint("ping", __name__)
api = Api(ping_bp)


class Ping(Resource):
    def get(self):
        return {"status": "success", "message": "pong!"}


api.add_resource(Ping, "/ping")
