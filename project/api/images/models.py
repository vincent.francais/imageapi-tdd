"""
File: /home/vincent/Documents/Projets/imageAPI-tdd/project/api/models.py
Created Date: Tuesday December 10th 2019
Author: Vincent Français
-----
Last Modified:
Modified By:
-----
"""

# from sqlalchemy.sql import func

import os
from project import db
from flask_admin.contrib.fileadmin import FileAdmin
from sqlalchemy.sql import func


class Image(db.Model):
    __tablename__ = "images"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    filename = db.Column(db.String(128), nullable=False)
    uri = db.Column(db.String(128), nullable=False)
    features_vec = db.Column(db.LargeBinary, nullable=False)
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    def __init__(self, filename="", uri="", vec=b""):
        self.filename = filename
        # self.img_data = img_data
        self.uri = uri
        self.features_vec = vec

    def to_json(self):
        return {"id": self.id, "filename": self.filename, "uri": self.uri}


if os.getenv("FLASK_ENV") == "development":
    from project import admin
    from project.api.images.admin import ImagesAdminView

    admin.add_view(
        FileAdmin(os.getenv("IMAGE_STORAGE_LOCATION"), "/static/", name="Static Files")
    )
    admin.add_view(ImagesAdminView(Image, db.session))
