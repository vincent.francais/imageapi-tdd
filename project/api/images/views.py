"""
File: /home/vincent/Documents/Projets/imageAPI-tdd/project/api/images.py
Created Date: Tuesday December 10th 2019
Author: Vincent Français
-----
Last Modified:
Modified By:
-----
"""

import json
import os
from imghdr import what

import werkzeug
from flask import Blueprint
from flask import current_app as app
from flask import send_from_directory
from flask_restful import Api, Resource, reqparse
from numpy import float32 as np_float32
from numpy import frombuffer
from sqlalchemy import exc
from werkzeug.utils import secure_filename

from project import db
from project.api.features_extraction import MobileNet, get_minimum_distance
from project.api.images.models import Image
from project.utils import get_exif

# import logging

images_bp = Blueprint("images", __name__)
api = Api(images_bp)

model = MobileNet()

"""
Here we define the routes for the API.

  Route             | HTTP  |   Class           | Description
------------------------------------------------------------
/images             | GET   | ImagesList        | Send the list of images stored in database
/images             | POST  | ImagesList        | Send an image to server and stored it to database
/images/id          | GET   | Images            | Send the image stored in database with the corresponding id
/images/id/similar  | GET   | Image_similar     | Send the most similar image compared to id
/images/id/metadata | GET   | Image_metadata    | Send the image's metadata with the corresponding id
"""


class Image_similar(Resource):
    def get(self, image_id):
        response_object = {"status": "fail", "message": "image does not exist"}
        try:
            master_image = Image.query.filter_by(id=int(image_id)).first()
            if not master_image:
                return response_object, 404

            master_vector = frombuffer(master_image.features_vec, dtype=np_float32)

            vector_id_list = [
                (image.id, frombuffer(image.features_vec, dtype=np_float32))
                for image in Image.query.all()
            ]
            app.logger.debug(f"There are {len(vector_id_list)} in the database.")

            if len(vector_id_list) == 1:
                response_object["message"] = "There only one image in database"
                return response_object, 400

            similar_id = get_minimum_distance(master_vector, vector_id_list)
            similar_image = Image.query.filter_by(id=int(similar_id)).first()

            app.logger.debug(
                f"Found similar image {similar_image.filename} with id {similar_image.id} with"
                f" master image {master_image.uri} id {master_image.id}"
            )

            return send_from_directory(app.config["SAVE_PATH"], similar_image.uri)
        except ValueError:
            return response_object, 404


class Image_metadata(Resource):
    """
    Handles the requests for an image metadata.
    """

    def get(self, image_id):
        response_object = {"status": "fail", "message": "image does not exist"}

        # If the image id exists in the DB, the Exif metadata
        # are extracted and sent in the response body.
        try:
            image = Image.query.filter_by(id=int(image_id)).first()
            if not image:
                return response_object, 404

            img_exif = get_exif(os.path.join(app.config["SAVE_PATH"], image.uri))

            return {"metadata": json.dumps(img_exif)}, 200
        except ValueError:
            return response_object, 404


class Images(Resource):
    """
    Handles the requests to list all the images in the database
    """

    def get(self, image_id):
        response_object = {"status": "fail", "message": "image does not exist"}

        # We first test if the image id exists.
        # Then we send the actual image if we found the corresponding entry
        try:
            image = Image.query.filter_by(id=int(image_id)).first()
            if not image:
                return response_object, 404
            else:
                # If we found an entry in the db we send back the image with a secure name
                return send_from_directory(app.config["SAVE_PATH"], image.uri)
        except ValueError:
            return response_object, 404


class ImagesList(Resource):
    def post(self):
        """
        Receive an image via POST to be stored in the database.
        The form has the following parameters:
            file (file): the image's binary data
            filename (string): the image filename
        """

        response_object = {"status": "fail", "message": "invalid payload"}

        # create the upload folder
        if not os.path.exists(app.config["SAVE_PATH"]):
            os.makedirs(app.config["SAVE_PATH"])

        # We expect a FileStorage object with the structure (byte_data, filename).
        parse = reqparse.RequestParser()
        parse.add_argument(
            "file", type=werkzeug.datastructures.FileStorage, location="files"
        )
        args = parse.parse_args()

        # if there is no image in the request return an error.
        if args["file"] is None or args["file"].filename is None:
            return response_object, 400

        # if the received file is not an image return an error.
        if what(args["file"]) is None:
            return response_object, 400

        image_FS = args["file"]
        filename = image_FS.filename
        img_uri = secure_filename(filename)
        # We first ensure that the filename does not exist in the DB.
        # The FileStorage object is then stored on the filesystem using a secure filename
        # which is stored in the DB.
        try:
            image = Image.query.filter_by(filename=filename).first()
            if not image:
                # We replace the cursor at the beginning of the stream, just in case.
                # The image stream from the FileStorage object is then used to get the image's features vector.
                image_FS.seek(0)
                features_vec = model.get_features_vector(image_FS.stream)
                image_FS.seek(0)
                image_FS.save(os.path.join(app.config["SAVE_PATH"], img_uri))

                # The image and its vector are stored in the database.
                db.session.add(
                    Image(filename=filename, uri=img_uri, vec=features_vec.tobytes())
                )
                db.session.commit()

                response_object["message"] = f"image {filename} was added!"
                response_object["status"] = "success"
                return response_object, 201
            else:
                response_object[
                    "message"
                ] = f"filename {filename} already exists in database"
                return response_object, 400
        except exc.IntegrityError:
            db.session.rollbacl()
            return response_object, 400

    def get(self):
        """
        Simply list all the images stored in the database and return it.
        """
        response_object = {
            "status": "success",
            "data": {"images": [image.to_json() for image in Image.query.all()]},
        }
        return response_object, 200


api.add_resource(ImagesList, "/images")
api.add_resource(Images, "/images/<image_id>")
api.add_resource(Image_metadata, "/images/<image_id>/metadata")
api.add_resource(Image_similar, "/images/<image_id>/similar")
