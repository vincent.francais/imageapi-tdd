"""
File: /home/vincent/Documents/Projets/imageapi-tdd/project/api/images/admin.py
Created Date: Sunday December 15th 2019
Author: Vincent Français
-----
Last Modified:
Modified By:
-----
"""


from flask_admin.contrib.sqla import ModelView


class ImagesAdminView(ModelView):
    can_create = False
    column_exclude_list = ["features_vec"]
    column_filters = (
        "uri",
        "filename",
    )
    column_editable_list = (
        "filename",
        "uri",
    )
    column_searchable_list = (
        "filename",
        "uri",
    )
    column_sortable_list = (
        "filename",
        "uri",
    )
    column_default_sort = ("uri", True)
