"""
File: features_extraction.py
File Created: Thursday, 1st January 1970 1:00:00 am
Author: Vincent Français (vincent.francais@gmail.com)
-----
Last Modified: Thursday, 12th December 2019 3:51:29 pm
Modified By: Vincent Français (vincent.francais@gmail.com>)
-----
"""

from keras.applications.mobilenet_v2 import preprocess_input
from keras.models import load_model
from numpy import asarray, expand_dims
from numpy import inf as np_inf
from numpy import where
from numpy.linalg import norm
from PIL import Image


class MobileNet:
    """
    Small wrapper the Keras/TF convolutional neural network. The model is
    loaded from an hdf5 file stored on the filesystem.
    The model used is MobileNetV2 trained on the ImageNet dataset, with an input shape of 224x224.
    """

    def __init__(self):
        self.model = load_model("/usr/src/app/project/mobilenet.h5", compile=False)

    def get_features_vector(self, image_path):
        """
        Return the features vector extracted from the model's prediction.
        The input image is handled by PIL and resized to (224,224).
        """
        img = Image.open(image_path)
        img = img.resize((224, 224))
        img_data = expand_dims(img, axis=0)
        img_data = preprocess_input(img_data)
        features_vec = self.model.predict(img_data)
        return features_vec.flatten()


def get_L2_norm(vec1, vec2):
    return norm(vec1 - vec2, ord=2)


def get_minimum_distance(master_vec, vec_id_list):
    """
    Return the image id most similar to master image.
    vec_id_list is a list of tuple (id, features_vector)
    """

    # Compute the distance vector
    vec_id_list = asarray(vec_id_list)
    distance_array = asarray(
        [get_L2_norm(master_vec, vec) for vec in vec_id_list[:, 1]]
    )

    # Put the distance corresponding to our master image, which is 0, to np.inf
    # so that it won't be returned by argmin()
    index = where(distance_array == 0.0)[0][0]
    distance_array[index] = np_inf

    # Return the id of the similar image
    similar_image_id = vec_id_list[distance_array.argmin()][0]
    return similar_image_id
