"""
File: /home/vincent/Documents/Projets/imageAPI-tdd/project/tests/unit/test_images.py
Created Date: Tuesday December 10th 2019
Author: Vincent Français
-----
Last Modified:
Modified By:
-----
"""

import json
import os
from io import BytesIO
from shutil import copy2

from numpy import asarray

from project.tests.utils import add_image, recreate_db
from project.utils import get_exif


def test_add_image(test_app, test_database):
    recreate_db()
    img_path = "/usr/src/app/project/tests/image_test_metadata.jpg"
    data = {}
    filename = "test01.jpg"

    # Here we a test image stored in the filesystem
    # The file is converted to bytes and send to the API

    data["file"] = (BytesIO(open(img_path, "rb").read()), filename)
    data["filename"] = filename

    client = test_app.test_client()
    resp = client.post("/images", data=data, content_type="multipart/form-data")

    data = json.loads(resp.data.decode())
    assert resp.status_code == 201
    assert "image test01.jpg was added!" in data["message"]
    assert "success" in data["status"]


def test_add_image_not_an_image(test_app, test_database):

    # We create a fake image from a simple string via BytesIO
    not_an_image = BytesIO(b"This is not an image")
    data = {}
    filename = "not_an_image.jpg"

    data["file"] = (not_an_image, filename)
    data["filename"] = filename
    client = test_app.test_client()

    resp = client.post("/images", data=data, content_type="multipart/form-data")

    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "invalid payload" in data["message"]
    assert "fail" in data["status"]


def test_add_image_invalid_json(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/images", data=json.dumps({}), content_type="multipart/form-data"
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "invalid payload" in data["message"]
    assert "fail" in data["status"]


def test_add_image_invalid_json_keys(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/images",
        data=json.dumps({"filename": "missing file"}),
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "invalid payload" in data["message"]
    assert "fail" in data["status"]


def test_add_image_duplicate_name(test_app, test_database):
    client = test_app.test_client()
    img_path = "/usr/src/app/project/tests/image_test_metadata.jpg"
    data = {}
    filename = "test01.jpg"

    # The image stored in the FS is converted to bytes and send twice

    data["file"] = (BytesIO(open(img_path, "rb").read()), filename)
    data["filename"] = filename
    resp = client.post("/images", data=data, content_type="multipart/form-data")

    data = {}
    filename = "test01.jpg"
    data["file"] = (BytesIO(open(img_path, "rb").read()), filename)
    data["filename"] = filename
    resp = client.post("/images", data=data, content_type="multipart/form-data")

    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "filename test01.jpg already exists in database" in data["message"]
    assert "fail" in data["status"]


def test_get_similar_image(test_app, test_database, test_model):
    """
    Test for the /images/id/similar route.
    Image 3999763.jpg should be returned as the similar image of 3999761.jpg
    """
    recreate_db()

    # We select the image to test and its similar image, and two others images
    test_images_folder = "/usr/src/app/project/tests/test-images"
    images_totest_list = ["3999761.jpg", "3999763.jpg", "3999709.jpg", "3999701.jpg"]

    db_ids = []

    # Insert the images in the database and transfer thes files to the upload folder
    for img_name in images_totest_list:
        img_path = os.path.join(test_images_folder, img_name)
        copy2(img_path, os.path.join(test_app.config["SAVE_PATH"], img_name))
        features_vec = test_model.get_features_vector(img_path)
        image = add_image(filename=img_name, uri=img_name, vec=features_vec)
        db_ids.append(image.id)

    grount_similar_path = os.path.join(test_images_folder, images_totest_list[1])
    with open(grount_similar_path, "rb") as img_stream:
        grount_similar_bytes = BytesIO(img_stream.read())

    client = test_app.test_client()
    resp = client.get(f"/images/{db_ids[0]}/similar")

    assert resp.status_code == 200
    assert resp.data == grount_similar_bytes.read()

    for img_name in images_totest_list:
        os.remove(os.path.join(test_app.config["SAVE_PATH"], img_name))


def test_get_similar_only_one_stored(test_app, test_database, test_model):
    """
    Test for the case where we request a similar image
    when only one image is stored in database.
    """
    recreate_db()

    img_path = "/usr/src/app/project/tests/test-images/3999761.jpg"
    filename = "3999761.jpg"
    copy2(img_path, os.path.join(test_app.config["SAVE_PATH"], filename))
    features_vec = test_model.get_features_vector(img_path)
    image = add_image(filename=filename, uri=filename, vec=features_vec)

    client = test_app.test_client()
    resp = client.get(f"/images/{image.id}/similar")

    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "There only one image in database" in data["message"]
    os.remove(os.path.join(test_app.config["SAVE_PATH"], filename))


def test_single_image(test_app, test_database, test_model):
    """
    Test of retrieving a single image from the API.
    We first copy a test image stored in the FS to the upload folder to
    mimic a storage from the POST method.
    Then we test if the returned image's bytes are equal to the test image bytes.
    """
    img_path = "/usr/src/app/project/tests/image_test_metadata.jpg"
    filename = "test01.jpg"
    copy2(img_path, os.path.join(test_app.config["SAVE_PATH"], filename))
    features_vec = test_model.get_features_vector(img_path)

    with open(img_path, "rb") as img_stream:
        img_byte = BytesIO(img_stream.read())

    image = add_image(filename=filename, uri=filename, vec=features_vec)

    img_byte.seek(0)
    client = test_app.test_client()
    resp = client.get(f"/images/{image.id}")

    assert resp.status_code == 200
    assert resp.data == img_byte.read()
    os.remove(os.path.join(test_app.config["SAVE_PATH"], filename))


def test_single_image_no_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/images/blah")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "image does not exist" in data["message"]
    assert "fail" in data["status"]


def test_single_image_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/images/999")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "image does not exist" in data["message"]
    assert "fail" in data["status"]


def test_list_all_images(test_app, test_database):
    recreate_db()
    client = test_app.test_client()

    # We insert fake images in the database
    # As we want to retrieve only the list of fils from the database we don't need to copy the file to upload folder
    fake_vec = asarray([1.0, 2.0, 3.0]).tobytes()
    add_image("fake_img1", "fake_img1_uri", fake_vec)
    add_image("fake_img2", "fake_img2_uri", fake_vec)
    resp = client.get("/images")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data["data"]["images"]) == 2
    assert "fake_img1" in data["data"]["images"][0]["filename"]
    assert "fake_img1_uri" in data["data"]["images"][0]["uri"]
    assert "fake_img2" in data["data"]["images"][1]["filename"]
    assert "fake_img2_uri" in data["data"]["images"][1]["uri"]


def test_get_metadata(test_app, test_database, test_model):
    recreate_db()
    client = test_app.test_client()

    # Insert test image in database
    img_path = "/usr/src/app/project/tests/image_test_metadata.jpg"
    filename = "test01.jpg"
    features_vec = test_model.get_features_vector(img_path)
    copy2(img_path, os.path.join(test_app.config["SAVE_PATH"], filename))
    image = add_image(filename=filename, uri=filename, vec=features_vec)
    source_metadata = get_exif(os.path.join(test_app.config["SAVE_PATH"], filename))

    resp = client.get(f"/images/{image.id}/metadata")
    data = json.loads(resp.data.decode())

    assert resp.status_code == 200
    assert json.dumps(source_metadata) == data["metadata"]
    os.remove(os.path.join(test_app.config["SAVE_PATH"], filename))


def test_get_metadata_no_exif(test_app, test_database, test_model):
    recreate_db()
    client = test_app.test_client()

    # Insert test image in database
    img_path = "/usr/src/app/project/tests/image-no-metadata.jpg"
    filename = "image-no-metadata.jpg"
    features_vec = test_model.get_features_vector(img_path)
    copy2(img_path, os.path.join(test_app.config["SAVE_PATH"], filename))
    image = add_image(filename=filename, uri=filename, vec=features_vec)
    source_metadata = get_exif(os.path.join(test_app.config["SAVE_PATH"], filename))

    resp = client.get(f"/images/{image.id}/metadata")
    data = json.loads(resp.data.decode())

    assert resp.status_code == 200
    assert json.dumps(source_metadata) == data["metadata"]
    os.remove(os.path.join(test_app.config["SAVE_PATH"], filename))


def test_get_metadata_no_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/images/blah/metadata")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "image does not exist" in data["message"]
    assert "fail" in data["status"]
