"""
File: /home/vincent/Documents/Projets/imageAPI-tdd/project/tests/utils.py
Created Date: Wednesday December 11th 2019
Author: Vincent Français
-----
Last Modified:
Modified By:
-----
"""


from project import db
from project.api.images.models import Image


def add_image(filename, uri, vec):
    image = Image(filename=filename, uri=uri, vec=vec)
    db.session.add(image)
    db.session.commit()
    return image


def recreate_db():
    db.session.remove()
    db.drop_all()
    db.create_all()
