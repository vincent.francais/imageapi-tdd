"""
File: /home/vincent/Documents/Projets/imageAPI-tdd/project/tests/conftest.py
Created Date: Tuesday December 10th 2019
Author: Vincent Français
-----
Last Modified:
Modified By:
-----
"""

import pytest

from project import create_app, db
from project.api.features_extraction import MobileNet


@pytest.fixture(scope="module")
def test_app():
    app = create_app()
    app.config.from_object("project.config.TestingConfig")
    with app.app_context():
        yield app


@pytest.fixture(scope="module")
def test_database():
    db.create_all()
    yield db
    db.session.remove()
    db.drop_all()


@pytest.fixture(scope="module")
def test_model():
    model = MobileNet()
    yield model
