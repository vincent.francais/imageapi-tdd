"""
File: /home/vincent/Documents/Projets/imageAPI-tdd/project/tests/functional/test_ping.py
Created Date: Tuesday December 10th 2019
Author: Vincent Français
-----
Last Modified:
Modified By:
-----
"""

import json


def test_ping(test_app):
    client = test_app.test_client()
    resp = client.get("/ping")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert "pong" in data["message"]
    assert "success" in data["status"]
