"""
File: /home/vincent/Documents/Projets/imageapi-tdd/project/tests/test_keras_model.py
Created Date: Saturday December 14th 2019
Author: Vincent Français
-----
Last Modified:
Modified By:
-----
"""

from project.api import features_extraction


def test_get_features_vector(test_app, test_database, test_model):
    """
    This test ensures that the features vector returned by neural network has the correct dimension.
    """
    img = "/usr/src/app/project/tests/test-images/3999761.jpg"
    vec = test_model.get_features_vector(img)

    assert vec.shape == (62720,)


def test_get_L2_distance(test_app, test_database, test_model):
    """
    This test ensures that the neural network return the corrrect L2 distance of two select images
    """
    vec1 = test_model.get_features_vector(
        "/usr/src/app/project/tests/test-images/3999761.jpg"
    )
    vec2 = test_model.get_features_vector(
        "/usr/src/app/project/tests/test-images/3999761.jpg"
    )

    assert vec1.shape == vec2.shape

    assert features_extraction.get_L2_norm(vec1, vec2) == 0.0
