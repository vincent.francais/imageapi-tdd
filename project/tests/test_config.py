"""
File: /home/vincent/Documents/Projets/imageAPI-tdd/project/tests/test_config.py
Created Date: Tuesday December 10th 2019
Author: Vincent Français
-----
Last Modified:
Modified By:
-----
"""

import os


def test_development_config(test_app):
    test_app.config.from_object("project.config.DevelopmentConfig")
    assert test_app.config["SECRET_KEY"] == "perfect-memory-seems-cool"
    assert not test_app.config["TESTING"]
    assert test_app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get("DATABASE_URL")
    assert test_app.config["SAVE_PATH"] == os.environ.get("IMAGE_STORAGE_LOCATION")
    assert test_app.config["SAVE_PATH"] is not None


def test_testing_config(test_app):
    test_app.config.from_object("project.config.TestingConfig")
    assert test_app.config["SECRET_KEY"] == "perfect-memory-seems-cool"
    assert test_app.config["TESTING"]
    assert not test_app.config["PRESERVE_CONTEXT_ON_EXCEPTION"]
    assert test_app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get(
        "DATABASE_TEST_URL"
    )
    assert test_app.config["SAVE_PATH"] == os.environ.get("IMAGE_STORAGE_LOCATION")
    assert test_app.config["SAVE_PATH"] is not None


def test_production_config(test_app):
    test_app.config.from_object("project.config.ProductionConfig")
    assert test_app.config["SECRET_KEY"] == "perfect-memory-seems-cool"
    assert not test_app.config["TESTING"]
    assert test_app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get("DATABASE_URL")
    assert test_app.config["SAVE_PATH"] == os.environ.get("IMAGE_STORAGE_LOCATION")
    assert test_app.config["SAVE_PATH"] is not None
