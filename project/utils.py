"""
File: utils.py
File Created: Thursday, 1st January 1970 1:00:00 am
Author: Vincent Français (vincent.francais@gmail.com)
-----
Last Modified: Thursday, 12th December 2019 12:07:53 pm
Modified By: Vincent Français (vincent.francais@gmail.com>)
-----
"""

import logging
from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS


def get_exif(filedata):
    """
    Extract and return the image Exif metadata as a dictionary.
    The numeric keys are translated to literal keys.
    """
    image = Image.open(filedata)
    image.verify()
    exif = image._getexif()
    exif = _get_labeled_exif(exif)
    return exif


def _get_labeled_exif(exif):
    """
    Map the numeric tags to their corresponding name.
    """
    if not exif:
        return {}
    labeled = {}
    for (key, val) in exif.items():
        labeled[TAGS.get(key)] = val

    geotagging = _get_geotagging(exif)
    labeled["GPSInfo"] = geotagging
    return labeled


def _get_geotagging(exif):
    """
    Map numeric GPS tag to their coresponding name.
    """
    if not exif:
        raise ValueError("No EXIF metadata found")

    geotagging = {}
    for (idx, tag) in TAGS.items():
        if tag == "GPSInfo":
            if idx not in exif:
                return {}

            for (key, val) in GPSTAGS.items():
                if key in exif[idx]:
                    geotagging[val] = exif[idx][key]

    return geotagging


def get_module_logger(mod_name):
    """
    To use this, do logger = get_module_logger(__name__)
    """
    logger = logging.getLogger(mod_name)
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        "%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger
