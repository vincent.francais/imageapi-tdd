"""
File: /home/vincent/Documents/Projets/imageAPI-tdd/project/config.py
Created Date: Tuesday December 10th 2019
Author: Vincent Français
-----
Last Modified:
Modified By:
-----
"""

import os  # new


class BaseConfig:
    """Base configuration"""

    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = "perfect-memory-seems-cool"
    SAVE_PATH = os.getenv("IMAGE_STORAGE_LOCATION")


class DevelopmentConfig(BaseConfig):
    """Development configuration"""

    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")


class TestingConfig(BaseConfig):
    """Testing configuration"""

    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_TEST_URL")


class ProductionConfig(BaseConfig):
    """Production configuration"""

    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")
