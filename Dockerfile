# pull base image
# We use Ubuntu Core as it lighter than the basic image
FROM multiarch/ubuntu-core:x86_64-bionic

ARG DEBIAN_FRONTEND=noninteractive

# Install and set up Python3
RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3.6 python \
  && pip3 install --upgrade pip

RUN apt-get install tzdata -y

# install dependencies
RUN apt-get update && \
    apt-get install -y apt-utils gcc musl-dev && \
    apt-get install -y postgresql && \
    apt-get install -y netcat-openbsd && \
    apt-get install -y libhdf5-dev && \
    apt-get install -y libjpeg-dev libfreetype6 libfreetype6-dev zlib1g-dev

# set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

# set working directory
WORKDIR /usr/src/app

# add and install requirements
COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip3 install -r requirements.txt


# Copy test images
COPY ./project/tests/image_test_metadata.jpg /usr/src/app/project/tests/image_test_metadata.jpg
COPY ./project/tests/test-images/*.jpg /usr/src/app//project/tests/test-images/

# Copy the Keras model
COPY ./project/mobilenet.h5  /usr/src/app/project/mobilenet.h5

# add entrypoint.sh
COPY ./entrypoint.sh /usr/src/app/entrypoint.sh
RUN chmod +x /usr/src/app/entrypoint.sh

# add app
COPY . /usr/src/app